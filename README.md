How to use the app ?

DEPENDENCIES

- kivy, pillow


-- Linux --

1) clone the repo

2) go inside adventphoto folder

3) source the env: source env/bin/activate

4) make sure you have the desired photo in photos folder

3) run app by: sudo python main.py

4) Complete the form and press ”Creează”

5) You'll find your output in the photos folder at photos/output.jpg


-- Android -- 

1) download the adventphoto-0.1-armeabi-v7a-debug.apk from bin folder

2) Go to settings > Applications

3) Find AdventPhoto and enable Storage permission under Permissions

4) run the app 

5) you'll find the output in your Gallery


-- IOS --

Read buildozer documentation for IOS 

https://buildozer.readthedocs.io/en/latest/


-- MACOS --

Use the linux steps


-- WINDOWS --

Read the kivy documentation
https://kivy.org/doc/stable/guide/packaging-windows.html
