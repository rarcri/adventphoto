# TODO  Add Insta-Story size to photo.py 
#       Add Insta-Post size to photo.py 1/1
#       Add Facebook-story size to photo.py


import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.gridlayout import GridLayout
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.filechooser import FileChooser, FileChooserIconLayout, FileChooserListLayout
from kivy.properties import ObjectProperty
from photo import createPhoto

class Main(Screen):
    img = ObjectProperty(None)
    txt = ObjectProperty(None)
    ref = ObjectProperty(None)
    logo_color = ObjectProperty(None)
    d7_color = ObjectProperty(None)

    def btn(self):
        print(f'{self.img.text}\n{self.txt.text}\n{self.ref.text}\n{self.logo_color.text}\n{self.d7_color.text}')
        createPhoto(self.img.text, self.txt.text, self.ref.text, self.logo_color.text, self.d7_color.text) 


class choosePhoto(Screen):
    pass

class editPhoto(Screen):
    pass

class PopupContent(GridLayout):
    pass



class AdventPhoto(App):
    def build(self):
        sm = ScreenManager()
        sm.add_widget(Main(name='main'))
        sm.add_widget(editPhoto(name='edit'))
        sm.add_widget(choosePhoto(name='choose'))
        return sm 


if __name__ == "__main__":
    AdventPhoto().run()


