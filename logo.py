from PIL import Image


def get_logo(argument):
    switcher = {
    0: "winter",
    1: "white",
    2: "velvet",
    3: "treefrog",
    4: "scarlett",
    5: "night",
    6: "ming",
    7: "lily",
    8: "iris",
    9: 'grapevine',
    10: 'forest',
    11: 'emperor',
    12: 'earth',
    13: 'denim',
    14: 'cave',
    15: 'campfire',
    16: 'bluejay',
    17: 'black'
    }
    return switcher.get(argument, "Invalid logo")
# Driver program
if __name__ == "__main__":
    print (get_logo(6))
    print (get_logo(8))
    print (get_logo(0)) 
    print (get_logo(19)) 
