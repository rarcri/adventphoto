from PIL import Image, ImageDraw, ImageFilter
from PIL import ImageFont
from datetime import datetime, timedelta
import textwrap
import shutil
import os.path
from math import sqrt


text='„Eu, Eu vă mângâi. Dar cine eşti tu, ca să te temi de omul cel muritor, şi de fiul omului, care trece ca iarba, şi să uiţi pe Domnul, care te-a făcut, care a întins cerurile şi a întemeiat pământul?”'

def createPhoto(
    photo='earth.jpg',
    text_content=text,
    text_reference='Isaia 51:12-13',
    logo_color = 'white',
    d7_color = 'steelblue'):

    

    # OPENING IMAGES
    img = Image.open('./photos/'+photo)
    logo = Image.open(f'./logos/adventist-symbol--{logo_color}.png')


    # BOXBLUR #
    img = img.filter(ImageFilter.BoxBlur(4))

    # Getting The x Coordinate #
    xlocation = img.width - int(img.width/7) 

    # Resizing the logo #
    logo_size = int(img.width/7)#-int(10/100 * int(img.width/7))
    rlogo = logo.resize((logo_size, logo_size))

    # DRAW RECTANGLE #
    draw = ImageDraw.Draw(img)
    draw.rectangle([(xlocation,0),(img.width,img.height)],fill=d7_color)


    # TEXT INPUT #
    t = text_content 

    reference = text_reference 

    
    # Calculation for text wrap #
    textWidth = img.width - int(img.width/7) 
    textHeight = img.height - int(img.height/7)


    # Text wrap logic #
    textLength = len(t)
    print(textLength)
    if(textLength < 28):
        textLength += 10

    wrapWidth=16
    textSize=int(sqrt(textWidth*textHeight/textLength))- 10
    print('text size:', textSize)
    print('text width:', textWidth)
    wrapWidth=int(textWidth/textSize) + int(0.05*textLength) + 7 
    print('wrap width:', wrapWidth)


    # WRAPPING #
    string = textwrap.TextWrapper(width=wrapWidth).fill(text=t)

    # FONT #
    myFont = ImageFont.truetype('fonts/OpenSans-B9K8.ttf', size=textSize)

    # TEXT #
    draw.multiline_text((int(xlocation/2), int(img.height/2-(0.1*img.height))),
    # draw.multiline_text((0,0),
    string,
    fill='white',
    font=myFont,
    anchor='mm',
    stroke_width=5,
    align='center',
    stroke_fill='black',
    embedded_color=False)


    # REFERECE TEXT #
    # temporary disabled for testing purposes
    draw.text((int(xlocation/2), img.height - int(img.height/9)),
    # draw.multiline_text((0,0),
    reference,
    fill='goldenrod',
    font=myFont,
    anchor='mm',
    stroke_width=5,
    stroke_fill='black',
    align='center'
    )


    # Adding over the main image the reduced logo #
    img.paste(rlogo, (xlocation,0), rlogo)

            # Getting the curent date
    date = datetime.now()


        
             # Saving the image #
#    img.save('/storage/emulated/0/AdventPhoto/output.jpg')
    img.save('./photos/output.jpg')
    if os.path.isfile(f'./history/postare-{date.day}-{date.month}-{date.year}--{date.hour}-{date.minute}'):
        img.save(f'./history/postare-{date.day}-{date.month}-{date.year}--{date.hour}-{date.minute}(1).jpg')
    else: 
        img.save(f'./history/postare-{date.day}-{date.month}-{date.year}--{date.hour}-{date.minute}.jpg')

